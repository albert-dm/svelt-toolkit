import { action } from '@storybook/addon-actions';

export default {
  title: 'Dropdown',
};

export const button = () => 
`<bp-dropdown > 
  <bp-btn slot="trigger" color="warning">...</bp-btn>
  <ul slot="items">
    <li>Primeiro item</li>
    <li>Segundo item</li>
  </ul>
</bp-dropdown>`;