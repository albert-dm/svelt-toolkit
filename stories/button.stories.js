import { action } from '@storybook/addon-actions';

export default {
  title: 'Button',
};

export const text = () => '<bp-btn>Hello World</bp-btn>';

/* export const emoji = () => ({
  Component: Button,
  props: {
    text: '😀 😎 👍 💯',
  },
  on: { click: action('clicked') },
}); */
